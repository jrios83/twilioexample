<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/bootstrap.min.css'/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/sms.css'/>"/>

<html>
	<head>
		<title>SMS Twilio Example</title>
	</head>
	<body>
		<h3 class="span12">SMS Twilio sample</h3>
		
		<div class="well span5">
			<c:set var="smsSuccessVis" value="style=display:none;"/>
			<c:set var="smsFailVis" value="style=display:none;"/>
			<c:if test="${smsSent}"> 
				<c:set var="smsSuccessVis" value=""/>
			</c:if>
		
			<c:if test="${smsFail}"> 
				<c:set var="smsFailVis" value=""/>
			</c:if>
		
			<div class="alert alert-success" <c:out value="${smsSuccessVis}"/>><c:out value="SMS Sent"/></div>
			<div class="alert alert-error"  <c:out value="${smsFailVis}"/>> <c:out value="Womp womp. SMS not sent."/></div>
		
			<form:form modelAttribute="sms" >
				<div>
					<form:label path="to" class="span1">To:</form:label>
					<form:input id="to" path="to" class="span3" maxlength="20"/>
					<form:errors path="to" class="span5 errors"/>
				</div>
				<div>
					<form:label path="to" class="span1">Body:</form:label>
					<form:input id="body" path="body" class="span3" maxlength="20"/>
					<form:errors path="body" class="span5 errors" />
				</div>
				<button class="btn btn-primary" type="submit"><c:out value="Send SMS"/></button>	
			</form:form>
		</div>
	</body>
</html>

