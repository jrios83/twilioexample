package org.jrios.samples;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/*
 * Simple Spring controller with a GET and POST Methods
 * 
 */

@Controller
@RequestMapping("/sms")
public class SmsController {

	@Autowired
	SmsGateway smsGateway;
	
	//Get the Form
	@RequestMapping(method = RequestMethod.GET)
	public String sms(Model model) {
		SmsMessage sms = new SmsMessage();
		model.addAttribute("sms", sms);
		return "sms";
	}
	
	//Post the Form
	@RequestMapping(method = RequestMethod.POST)
	public String sendSms(Model model, 
			@Valid @ModelAttribute("sms") SmsMessage sms,
			BindingResult results) {
		
		if(results.hasErrors()){
			model.addAttribute("smsFail", true);
			
		}else{
			smsGateway.send(sms);
			model.addAttribute("smsSent", true);
		}
		return "sms";
	}
	
}
