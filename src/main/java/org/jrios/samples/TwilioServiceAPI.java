package org.jrios.samples;

import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.verbs.TwiMLException;

//Just another interface. It really that easy.
public interface TwilioServiceAPI {

	public void sendSms(SmsMessage sms) throws TwilioRestException, TwiMLException;
	
}
