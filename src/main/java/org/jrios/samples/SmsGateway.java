package org.jrios.samples;

import java.util.concurrent.Future;

//Gateway interface for Spring Integration
public interface SmsGateway {

    public Future<?> send(SmsMessage sms);

}

