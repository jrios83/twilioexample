package org.jrios.samples;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Sms;
import com.twilio.sdk.verbs.TwiMLException;

@Component
public class TwilioSmsService implements TwilioServiceAPI{
	
	//Let Spring inject the values from our property file
	@Value("${sms.number}")
	String from;

	@Value("${sms.acct.ssid}")
	String acctSSID;
	
	@Value("${sms.token}")
	String token;
	
	//Implement out Interface that actually send the SMS 
	public void sendSms(SmsMessage sms) throws TwilioRestException, TwiMLException {

		    // Create a rest client
		    final TwilioRestClient client = new TwilioRestClient(acctSSID, token);
		    
		    // Get the main account
		    final Account mainAccount = client.getAccount();

		    // Send an SMS
		    final SmsFactory smsFactory = mainAccount.getSmsFactory();
		    final Map<String, String> smsParams = new HashMap<String, String>();
		    
		    smsParams.put("To", sms.getTo()); 	// Replace with a valid phone number
		    smsParams.put("From", from); 		// Replace with a valid phone number in your account
		    smsParams.put("Body", sms.getBody());
		    
		    Sms create = smsFactory.create(smsParams);
		    
		    //Get the id of SMS message so we can retrieve the status of the message
		    String ssid = create.getSid();
		    
		    String status = create.getStatus();
		    Sms message = client.getAccount().getSms(ssid);
		    while(status == null || (!status.equalsIgnoreCase("sent") && !status.equalsIgnoreCase("failed"))){
		    	 status = message.getStatus();
		    	 System.err.println(status);
		    }
		    
		    StringBuilder stringBuilder = new StringBuilder();
		    stringBuilder.append("Message To:" + message.getTo())
		    .append("\nMessage Body:" + message.getBody())
		    .append("\nFinal status " + status)
		    .append("\nPrice:" +  (message.getPrice() == null? "FREE!" : message.getPrice()));
		    System.err.println(stringBuilder);
		  }
}
