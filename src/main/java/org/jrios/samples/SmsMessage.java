package org.jrios.samples;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * Simple POJO representing the SMS message
 */
public class SmsMessage implements Serializable{
	
	private static final long serialVersionUID = 66147744135165728L;
	
	@NotEmpty(message="Need a person to send to.")
	private String to ;
	private String from;
	
	@NotEmpty(message="Need a message to send.")
	private String body;

    public SmsMessage() {
    }

    public String getTo() {
    	return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

	public String getFrom() {
    	return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getBody() {
    	return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

}
